package Menu;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author Javier Duran
 */
public class Cotizacion {
     // Atributos de la clase
    private int noCotizacion;
    private String descripcion;
    private float precio;
    private float porcentajeInicial;
    private int plazo;

    // Metodos constructores
    //Por Omision
    public Cotizacion() {
        this.noCotizacion = 0;
        this.descripcion = "";
        this.precio = 0.0f;
        this.porcentajeInicial = 0.0f;
        this.plazo = 0;
    }

    public Cotizacion(int noCotizacion, String descripcion, float precio, float porcentajeInicial, int plazo) {
        this.noCotizacion = noCotizacion;
        this.descripcion = descripcion;
        this.precio = precio;
        this.porcentajeInicial = porcentajeInicial;
        this.plazo = plazo;
    }
    
    //Copia
    
    public Cotizacion(Cotizacion otro) {
        this.noCotizacion =otro.noCotizacion;
        this.descripcion =otro.descripcion;
        this.precio =otro.precio;
        this.porcentajeInicial =otro.porcentajeInicial;
        this.plazo =otro.plazo;
    }

    // Metodo get/set
    
    public int getNoCotizacion() {
        return noCotizacion;
    }

    public void setNoCotizacion(int noCotizacion) {
        this.noCotizacion = noCotizacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajeInicial() {
        return porcentajeInicial;
    }

    public void setPorcentajeInicial(float porcentajeInicial) {
        this.porcentajeInicial = porcentajeInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    // Metodos de comportamiento
    public float calcularPagoInicial() {
        float pagoin = 0.0f;
        pagoin = precio * (porcentajeInicial / 100);
        return pagoin;
    }

    public float calcularTotalFinanciar() {
        float totalfi = 0.0f;
        totalfi = precio - calcularPagoInicial();
        return totalfi;
    }

    public float calcularPagoMensual() {
        float pagome = 0.0f;
        pagome = calcularTotalFinanciar() / plazo;
        return pagome;
    }     
}
