package Menu;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import javax.swing.JOptionPane;
/**
 *
 * @author Javier Duran
 */
public class jdlgCotizacion extends javax.swing.JDialog {
    
    /**
     * Creates new form jdlgCotizacion
     */
    public jdlgCotizacion(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.limpiar();
        this.deshabilitar();
        this.setSize(860,700);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtPorcentaje = new javax.swing.JTextField();
        txtFolio = new javax.swing.JTextField();
        txtDescripcion = new javax.swing.JTextField();
        txtPrecio = new javax.swing.JTextField();
        cmbPlazos = new javax.swing.JComboBox<>();
        btnCancelar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        btnCalcular = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnNuevo = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        lblMensual = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lblInicial = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cotizacion");
        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Plazos :");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(200, 250, 90, 30);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Folio  :");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(200, 40, 60, 30);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Descripción del Automovil :");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(40, 90, 230, 30);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Precio :");
        getContentPane().add(jLabel4);
        jLabel4.setBounds(200, 140, 60, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Porcentaje de Pago Inicial :");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(40, 200, 230, 30);
        getContentPane().add(txtPorcentaje);
        txtPorcentaje.setBounds(270, 200, 210, 30);

        txtFolio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtFolioActionPerformed(evt);
            }
        });
        getContentPane().add(txtFolio);
        txtFolio.setBounds(270, 40, 230, 30);

        txtDescripcion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDescripcionActionPerformed(evt);
            }
        });
        getContentPane().add(txtDescripcion);
        txtDescripcion.setBounds(270, 90, 300, 30);
        getContentPane().add(txtPrecio);
        txtPrecio.setBounds(270, 140, 210, 30);

        cmbPlazos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "12 Meses", "24 Meses", "36 Meses", "48 Meses", "60 Meses" }));
        cmbPlazos.setEnabled(false);
        cmbPlazos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbPlazosActionPerformed(evt);
            }
        });
        getContentPane().add(cmbPlazos);
        cmbPlazos.setBounds(300, 250, 80, 22);

        btnCancelar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(100, 540, 160, 50);

        btnLimpiar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(330, 540, 130, 50);

        btnCerrar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(530, 540, 130, 50);

        btnCalcular.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnCalcular.setText("Calcular");
        btnCalcular.setEnabled(false);
        btnCalcular.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalcularActionPerformed(evt);
            }
        });
        getContentPane().add(btnCalcular);
        btnCalcular.setBounds(630, 180, 140, 50);

        btnGuardar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.setEnabled(false);
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(630, 110, 140, 50);

        btnNuevo.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(630, 40, 130, 50);

        jPanel1.setBackground(new java.awt.Color(204, 255, 153));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "|Calculos|", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 18), new java.awt.Color(255, 51, 51))); // NOI18N
        jPanel1.setLayout(null);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Pago Inicial ");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(60, 60, 110, 30);

        lblMensual.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblMensual.setText("$");
        jPanel1.add(lblMensual);
        lblMensual.setBounds(490, 60, 130, 30);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel8.setText("Plazos :");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(200, 250, 90, 30);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel9.setText("Total a financiar");
        jPanel1.add(jLabel9);
        jLabel9.setBounds(190, 130, 150, 30);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel10.setText("Pago Mensual");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(390, 60, 130, 30);

        lblInicial.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblInicial.setText("$");
        jPanel1.add(lblInicial);
        lblInicial.setBounds(150, 60, 130, 30);

        lblTotal.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblTotal.setText("$");
        jPanel1.add(lblTotal);
        lblTotal.setBounds(300, 130, 130, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(60, 300, 640, 200);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbPlazosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbPlazosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbPlazosActionPerformed

    private void btnCalcularActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalcularActionPerformed
            this.lblInicial.setText(String.valueOf(co.calcularPagoInicial()));
            this.lblTotal.setText(String.valueOf(co.calcularTotalFinanciar()));
            this.lblMensual.setText(String.valueOf(co.calcularPagoMensual()));

        // TODO add your handling code here:
       

    }//GEN-LAST:event_btnCalcularActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        //validar
        if(this.txtPrecio.getText().matches("") 
                || this.txtFolio.getText().matches("")
                 || this.txtDescripcion.getText().matches("")
                    ||this.txtPorcentaje.getText().matches("")){
            
            JOptionPane.showMessageDialog(this, "Faltó capturar información");
        }
        else{
            co.setNoCotizacion(Integer.parseInt(this.txtFolio.getText()));
            co.setDescripcion(this.txtDescripcion.getText());
            co.setPorcentajeInicial(Float.parseFloat(this.txtPorcentaje.getText()));
            co.setPrecio(Float.parseFloat(this.txtPrecio.getText()));
            
            
            //plazos
            int plazo = this.cmbPlazos.getSelectedIndex();
            
            switch (plazo){
                case 0: plazo = 12; break;
                case 1: plazo = 24; break;
                case 2: plazo = 36; break;
                case 3: plazo = 48; break;
                case 4: plazo = 60; break;
            }
            
            co.setPlazo(plazo);
            
            this.btnCalcular.setEnabled(true);
        }

    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        this.txtDescripcion.setEnabled(true);
        this.txtFolio.setEnabled(true);
        this.txtPrecio.setEnabled(true);
        this.txtPorcentaje.setEnabled(true);
        this.cmbPlazos.setEnabled(true);
        this.btnGuardar.setEnabled(true);

        // TODO add your handling code here:
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void txtDescripcionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDescripcionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDescripcionActionPerformed

    private void txtFolioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtFolioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFolioActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        limpiar();
        this.btnCalcular.setEnabled(true);
        
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        limpiar();
        deshabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int res = JOptionPane.showConfirmDialog(this,"Desea Salir","Cotizacion",JOptionPane.YES_NO_OPTION);
        this.setSize(600, 600);
        if (res == JOptionPane.YES_OPTION) this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jdlgCotizacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jdlgCotizacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jdlgCotizacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jdlgCotizacion.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jdlgCotizacion dialog = new jdlgCotizacion(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalcular;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JComboBox<String> cmbPlazos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblInicial;
    private javax.swing.JLabel lblMensual;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JTextField txtDescripcion;
    private javax.swing.JTextField txtFolio;
    private javax.swing.JTextField txtPorcentaje;
    private javax.swing.JTextField txtPrecio;
    // End of variables declaration//GEN-END:variables
    Cotizacion co = new Cotizacion();

     public void limpiar(){
        this.txtDescripcion.setText("");
        this.txtFolio.setText("");
        this.txtPrecio.setText("");
        this.txtPorcentaje.setText("");
        this.lblInicial.setText("");
        this.lblMensual.setText("");
        this.lblTotal.setText("");
        //combo box a 0
        this.cmbPlazos.setSelectedIndex(0);
        
    }
     
     public void deshabilitar(){
         this.txtDescripcion.setEnabled(false);
         this.txtFolio.setEnabled(false);
         this.txtPorcentaje.setEnabled(false);
         this.txtPrecio.setEnabled(false);
         this.cmbPlazos.setEnabled(false);
         this.btnCalcular.setEnabled(false);
         this.btnCalcular.setEnabled(false);
         
     }
}
